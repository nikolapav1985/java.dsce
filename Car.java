/**
*
* Car class
*
* used to represent different kinds of cars
*
*/
abstract class Car{
    public Engine engine; // car needs some kind of engine
    public Car(Engine engine){ // used to set engine of some car
        this.engine = engine;
    }
}
