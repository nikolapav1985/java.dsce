/**
*
* SUV class
*
* used to represent SUVs sport utility vehicles
*
*/
class SUV extends Car{
    public SUV(Engine engine){
        super(engine); // use parent constructor to set engine -----super-----
    }
}
