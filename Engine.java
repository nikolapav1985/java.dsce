/**
*
* Engine class
*
* used to represent different kinds of engines
*
*/
class Engine{
    public String type; // type of engine e.g. diesel, electric, hybrid ...
    public Engine(String type){ // used to initialize object
        this.type=type; // set type of current object using -----this-----
    }
}
