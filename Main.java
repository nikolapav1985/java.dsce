/**
*
* Main class
*
* used to run example
*
*
* EXAMPLE OUTPUT
*
* SUV engine type: electric
*
* COMPILE
*
* javac Main.java
*
* RUN
*
* java Main
*
*
*/
class Main{
    public static void main(String[] args){
        Engine electric = new Engine("electric"); // make an electric engine
        Car suv = new SUV(electric); // make SUV using electric engine
        System.out.println("SUV engine type: "+suv.engine.type); // print type of SUV engine
    }
}
